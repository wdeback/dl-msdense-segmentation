def MSDenseNet(width, depth, n_input_channels=1, n_output_channels=1, drop_out=0.0, batch_norm=False):
    '''CNN for segmentation based on dilated convolutions and dense connections. 
    
    Ref: Pelt and Sethian, PNAS, 2018: http://www.pnas.org/content/115/2/254

    Args:
    - width: number of channels per layer
    - depth: number of hidden layers
    - n_input_channels: number of input channels (default: 1)
    - n_output_channels: number of output channels (default: 1)
    - drop_out: dropout rate (default: 0.0)
    - batch_norm: if True, adds batch normalization after every convolution (default: False) 

    Returns:
    - keras model

    '''
    from keras.layers import Input, Dropout, BatchNormalization, Conv2D, Concatenate
    from keras.models import Model
    from keras.optimizers import Adam
    from keras.regularizers import l2
    from keras.utils import np_utils
    from keras.losses import binary_crossentropy
    
    def convolution(n_filters, dilation, inputs, drop_out, name=None):
        if len(inputs) > 1:
            i = Concatenate()(inputs)
        else:
            i = inputs[0]
        c = Conv2D(filters=n_filters, dilation_rate=(dilation, dilation),
                                        kernel_size=(3,3), strides=(1,1), padding='same', 
                                        #kernel_regularizer=l2(0.000),
                                        activation='relu', use_bias=True, name=name)(i)
        if batch_norm:
            c = BatchNormalization()(c)
            
        if drop_out:
            c = Dropout(rate=drop_out)(c)
            
        return c

    inp = Input(shape=(None, None, n_input_channels))
    
    if batch_norm:
        bn = BatchNormalization()(inp)
        inputs = [bn]
    else:
        inputs = [inp]
    
    for i in range(depth):
        for j in range(width): 
            s_ij = ((i*width + j) % 10) + 1
            c = convolution(n_filters=1, dilation=s_ij, inputs=inputs, drop_out=drop_out, name='layer_{}_stride_{}'.format(i, s_ij))
            inputs.append(c)
        
    c = Concatenate()(inputs)
    o = Conv2D(filters=n_output_channels, kernel_size=(1,1), padding='same', activation='sigmoid')(c)
       
    return Model(inputs=[inp], outputs=[o])

